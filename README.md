# Jinja-to-file
Pypi link https://pypi.org/project/j2f/

Jinja2 to file command line

```
$ python j2f/j2f.py --vars vars.yml deploy.yml 
Traceback (most recent call last):
  File "j2f/j2f.py", line 58, in <module>
    rendered = jinja2file(args.template, template_vars)
  File "j2f/j2f.py", line 43, in jinja2file
    rendered = j2_env.get_template(template_file).render(template_vars)
  File "/home/jorge/.virtualenvs/j2f/lib/python3.5/site-packages/jinja2/environment.py", line 1008, in render
    return self.environment.handle_exception(exc_info, True)
  File "/home/jorge/.virtualenvs/j2f/lib/python3.5/site-packages/jinja2/environment.py", line 780, in handle_exception
    reraise(exc_type, exc_value, tb)
  File "/home/jorge/.virtualenvs/j2f/lib/python3.5/site-packages/jinja2/_compat.py", line 37, in reraise
    raise value.with_traceback(tb)
  File "/home/j2f/deploy.yml", line 147, in top-level template code
    component_gitrevision: "{{ gitrevision }}"

```
then add the missing var 'component_gitrevision' to vars.yml file 
```
- component_gitrevision: '123456'
```
and rerun the j2f script to render the template

## build binary
For an easy way to use, a portable binary can be build using setup-bin.py

```
python setup-bin.py build
```
