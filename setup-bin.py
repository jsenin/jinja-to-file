from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [], excludes = [])

base = 'Console'

executables = [
    Executable('j2f/j2f.py', base=base, targetName = 'j2f')
]

setup(name='j2f',
      version = '1.0',
      description = 'Renders Jinja2 templates using commandline',
      install_requires=['Jinja2', 'PyYaml'],
      options = dict(build_exe = buildOptions),
      executables = executables)
