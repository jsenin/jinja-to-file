# requires to install jinja2
# pip install jinja2

import os
import sys
import yaml
import argparse
import jinja2
from jinja2 import Environment, FileSystemLoader, StrictUndefined

def mergeall(mesh):
    merged = {}
    for item in mesh:
        merged.update(item)
    return merged

def yml2dict(source):
    if not source:
        return None
    with open(source, 'r') as f:
        try:
            content= yaml.safe_load(f)
            return mergeall(content)
        except yaml.YAMLError as exc:
            print("Error bad yaml {}".format(source))

class NullUndefined(jinja2.Undefined):
    def __int__(self):
        return 999999
    def __float__(self):
        return 999999
    def __str__(self):
        return "xxxxxxxxxxxxxxxxxxxxxxxx"


def jinja2file(source, template_vars):
    template_dir = os.path.dirname(os.path.abspath(source))
    # j2_env = Environment(loader=FileSystemLoader(template_dir), trim_blocks=True,  undefined=NullUndefined)
    j2_env = Environment(loader=FileSystemLoader(template_dir), trim_blocks=True,  undefined=StrictUndefined)
    template_file = os.path.basename(source)
    if not template_vars:
        template_vars = {}
    rendered = j2_env.get_template(template_file).render(template_vars)
    return rendered

def process_arguments():

    parser = argparse.ArgumentParser(description='Render jinja2 templates at commandline')
    parser.add_argument('template', help='template filename')
    parser.add_argument('--vars', type=str, nargs='?', help='yaml file with vars', dest='yaml_vars_file')

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = process_arguments()
    template_vars = yml2dict(args.yaml_vars_file)
    rendered = jinja2file(args.template, template_vars)
    print(rendered)
